const User = require('../model/user.model');

exports.create = function (req, res) {
    var createInfor = new User;
    createInfor.name = req.body.name;
    createInfor.email = req.body.email;
    createInfor.phone = req.body.phone;
    createInfor.age = req.body.age;
    console.log('create value,', createInfor);
    createInfor.save(function (err, docs) {
        if (!err)
            res.send({ doc: docs });
        else
            res.send(err);
    });
}

exports.getdata = function (req, res) {
    User.find(function (err, docs) {
        if (err) {
            res.send(err);
        }
        res.send({ docs });
    });
}

exports.updatebyid = function (req, res) {
    // User.findById(req.params.id, function (err, docs) {
    //     if (err) {
    //         res.send(err);
    //     }
    //     console.log('req params:', req.params);
    //     console.log('req body:', req.body);
    //     docs.name = req.body.name;
    //     docs.email = req.body.email;
    //     docs.phone = req.body.phone;
    //     docs.age = eq.body.age;
    //     docs.update(function (err, updatedDoc) {
    //         console.log('updatedDoc:', updatedDoc)
    //         res.send({ docs: updatedDoc })
    //     });
    // })
    console.log('req params:', req.params);
    console.log('req body:', req.body);
    User.findOneAndUpdate(
        { "_id": req.params.id },
        {
            $set: {
                "name": req.body.name,
                "email": req.body.email,
                "phone": req.body.phone,
                "age": req.body.age
            }
        },
        { upsert: true }
        , function (err, results) {
            if (!err)
                res.send({ id: results._id + " " + "updated record" });
            else
                res.send(err);
        })
}

exports.deletebyid = function (req, res) {
    User.remove({ _id: req.params.id }, function (err, docs) {
        if (err)
            res.send(err)
        res.send({ msg: "Record deleted from the data base" })
    })
}

exports.getdataifmatch = function (req, res) {
    console.log('inside the data match', req.body);
    User.aggregate([
        {
            "$match": { "name": req.body.name }
        }
    ], function (error, result) {
        if (!error)
            res.send({ data: result });
        else
            res.send(error)

    });
}