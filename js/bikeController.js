const bike = require('../model/bike.model');

exports.createBikeInfo = function (req, res) {
    var bikeinfo = new bike;
    bikeinfo.name = req.body.name;
    bikeinfo.price = req.body.price;
    bikeinfo.ownername = req.body.ownername;
    bikeinfo.registrationfrom = req.body.registrationfrom;
    bikeinfo.isloan = req.body.isloan;
    bikeinfo.loanamountpending = req.body.loanamountpending;
    bikeinfo.save(function (error, result) {
        if (!error)
            res.send({ data: result });
        else
            res.send(error);
    })
}