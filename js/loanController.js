const loan = require('../model/loan.model');

exports.createOwnerInfo = function (req, res) {
    var loaninfo = new loan;
    loaninfo.insurancecompany = req.body.insurancecompany;
    loaninfo.loanamount = req.body.loanamount;
    loaninfo.intrestrate = req.body.intrestrate;
    loaninfo.duration = req.body.duration;
    loaninfo.save(function (error, result) {
        if (!error)
            res.send({ data: result });
        else
            res.send(error);
    })
}