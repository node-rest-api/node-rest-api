const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String },
    age: { type: Number }
});

module.exports = mongoose.model('users', userSchema);
