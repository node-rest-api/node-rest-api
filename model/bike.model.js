const mongoose = require("mongoose");
var Schema = mongoose.Schema;

const bikeSchema = new Schema({
    name: { type: String, required: true },
    price: { type: Number },
    ownername: { type: String, required: true },
    registrationfrom: { type: String, required: true },
    isloan: { type: String },
    loanamountpending: { type: Number }
});

module.exports = mongoose.model('bikeInformation', bikeSchema);