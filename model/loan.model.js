const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var loanSchema = new Schema({
    insurancecompany: { type: String },
    loanamount: { type: Number },
    intrestrate: { type: String },
    duration: { type: String }
});

module.exports = mongoose.model('insurance', loanSchema);