var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors')
var mongoose = require('mongoose');
const dbConfig = require('./config/db.config.js');
const routing = require('./routes/routes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var port = process.env.PORT || 8000;

app.use(cors())
app.use('/', routing);
app.listen(port);
console.log('REST API is runnning at ' + port);

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
