const express = require('express');
const router = express.Router();

var User = require('../js/userController');
var bike = require('../js/bikeController');
var loan = require('../js/loanController');

/* User Rest API*/
router.post('/create', User.create);
router.get('/getall', User.getdata);
router.post('/update/:id', User.updatebyid);
router.delete('/delete/:id', User.deletebyid);
router.post('/getdataifmatch', User.getdataifmatch);

// bike Rest API
router.post('/createbike', bike.createBikeInfo);

// loan Rest API
router.post('/createowner', loan.createOwnerInfo);

module.exports = router;